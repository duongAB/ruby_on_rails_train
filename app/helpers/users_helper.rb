module UsersHelper

  def gravatar_for(user, options = { size: 80 })
    size = options[:size]
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://media-cdn.laodong.vn/storage/newsportal/2021/7/5/927516/Luu_Diec_Phi3.jpg?w=414&h=276&crop=auto&scale=both/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end
